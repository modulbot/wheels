# Robot's Wheels

## Description
![Photo of the printed part](KidzRoboWheel.jpg "A photo of the wheels to explain what this is all about..")

For a project with kids at Greentech.Berlin<sup>[1](#1)</sup>,
we are building a modular autonomously driving robot.
The first subproject is an intelligent rolling chassis<sup>[2](#2)</sup>,
 controlled by an ESP32<sup>[3](#3)</sup>.

This repository is only storing the 3D-design of the wheels.

There are two flavours of the wheel available. The first version
was just a flat surface where one could then cut a section of
a bicycle air tube and pull it over. The second version still works
the same way while improving the behaviour on uneven surfaces.
Even more importantly, it can be used as a continnous
track / caterpillar if put on a rubber band. We actually did cut a
bicycle tupe into small triangles and glued them together to form
such a band.

![Serving suggestion - photo of a chassis with continous track made from recycled bicycle tube](KidzRoboChassisContinousTrack.jpg "A photo of the different forms of wheels, the rounded edge can be used with continous tracks. Sort of a serving suggestion that is.")

## Installation

The wheels have a diameter of 40mm and were crafted with
FreeCAD<sup>[4](#4)</sup> to fit to an axle of an electric motor.

Without any modification the wheel fits on an axle of 1.9mm diameter.

In order to garantee a perfect fit, a clamp was added.

If needed you can open and edit the `.FCStd` file with FreeCAD.

In case you do not want to edit the diameters you can use the `.stl`
file, import it into your favorite slicer, and eventually print the wheel with
your 3D-printer of joice (tested with PrusaSlicer<sup>[5](#5)</sup> on a MK3S+<sup>[6](#6)</sup>).

After printing, just push the wheel over the axle and tighten the clamp with an
M3 screw (DIN 912) and nut.

To get more friction on a smooth surface you can cut a small slice of an old
bicycle hose and slip it over the wheel.


## Usage

We use the wheels to drive a rolling chassis for a modular robot project.
The details will also be published as soon as it is ready.

The wheel has 16 equally distributed holes around its center.
These holes can be used to monitor the
rotation of the wheel.

To only build a simple vehicle you can basically mount two e-motors onto
a stick, with a second stick (or e.g. some wire) used as a stabilizer "wheel".

Depending on the polarization of the current, the vehicle can drive forwards,
backwards, or turn around left and clock- or counterclockwise.

## Support / Contributing

For any questions or suggestions, please directly contact us (i.e. the authors, see below).

## Roadmap

This repository is responsible only for the wheels, so there are no big improvements planned here.
Future developments on the chassis or the modular robot itself will be made in separate repositories
respectively.

## Authors and acknowledgment

  * Michael Lustenberger: mic\_at\_greentech.berlin

## License

The work here is published unter the terms of the
"CERN Open Hardware Licence Version 2 - Permissive"<sup>[7](#7)</sup> (for details please see the LICENSE file).

## Project status

Stable and in use...

---

  1. <a name="1" href="https://greentech.berlin">https://greentech.berlin</a>
  1. <a name="2" href="https://gitlab.com/modulbot/chassis">https://gitlab.com/modulbot/chassis</a> (last access: 2023-04-16)
  1. <a name="3" href="https://docs.espressif.com/projects/esp-idf/en/latest/hw-reference/index.html">https://docs.espressif.com/projects/esp-idf/en/latest/hw-reference/index.html</a> (last access: 2022-07-23)
  1. <a name="4" href="https://freecadweb.org">https://freecadweb.org</a> (last access: 2022-07-23)
  1. <a name="5" href="https://www.prusa3d.com/page/prusaslicer_424/">https://www.prusa3d.com/page/prusaslicer_424/</a> (last access: 2022-07-23)
  1. <a name="6" href="https://www.prusa3d.com/product/original-prusa-i3-mk3s-3d-printer-3/">https://www.prusa3d.com/product/original-prusa-i3-mk3s-3d-printer-3/)</a> (last access: 2022-07-23)
  1. <a name="7" href="https://choosealicense.com/licenses/cern-ohl-p-2.0/">https://choosealicense.com/licenses/cern-ohl-p-2.0/</a> (last access: 2022-07-23)
